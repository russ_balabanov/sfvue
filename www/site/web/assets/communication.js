new Vue({
    delimiters: ['<%', '%>'],
    el: "#app",
    mounted : function () {
        for(var userId in dbUsers) {
            this.selectedUsers.push(dbUsers[userId]);
            this.selectedIds.push(dbUsers[userId].id);

            delete this.users[userId];
        }
    },
    data : {
        users : users,
        selectedUsers : [],
        selectedIds : [],
        dragId : null
    },
    methods : {
        dragTag : function (userId) {
            this.dragId = userId;
        },
        dropTag : function (event) {
            if (this.dragId) {
                this.addUser(this.dragId);

                delete this.users[this.dragId];
                this.dragId = null;
            }
        },
        addUser : function (userId) {
            if (this.selectedIds.indexOf(userId) === -1) {
                this.selectedIds.push(userId);
                this.selectedUsers.push(this.users[userId]);
            }
        },
        removeUser : function (userId) {
            var index = this.selectedIds.indexOf(userId);
            if (index >= 0) {
                this.selectedIds.splice(index, 1);
                var selectedUser = this.selectedUsers.splice(index, 1);
                this.users[userId] = selectedUser[0];
            }
        }
    },
    computed : {
        isEditAllowed : function() {
            return isEditAllowed === 1;
        }
    }
});
