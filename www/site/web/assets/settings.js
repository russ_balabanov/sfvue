var routePrefix = typeof(env) != "undefined" ? '/app_dev.php' : '';
console.log(routePrefix);

new Vue({
    delimiters: ['<%', '%>'],
    el: "#app",
    mounted : function () {
        var self = this;
        if (!this.init) {
            this.$http.get(routePrefix + '/find-users').then(function(response) {
                self.tableData = response.body;
            });
        }
    },
    data : {
        init : false,
        search : {
            user : null,
            role : null
        },
        userRoles : ['ROLE_READ', 'ROLE_EDIT', 'ROLE_DELETE'],
        columns : ['id', 'username', 'role', 'email'],
        tableData : []
    },
    methods : {
        isChecked : function (userRoles, role) {
            return userRoles.indexOf(role) !== -1;
        },
        updateForm : function (userId, event) {
            var self = this;
            var data = {roles : []};

            var form = event.target.form;
            var elements = form.elements['roles'];
            for (var i = 0; i < elements.length; i++) {
                if (elements[i].checked) {
                    data.roles.push(elements[i].value);
                }
            }

            if (!data.roles.length) {
                data.roles = null;
            }

            var options = {
                emulateJSON : true
            };
            this.$http.post(routePrefix + '/update-user/' + userId, {user : data }, options).then(function (response) {
                self.updateTableView();
            });
        },
        updateTableView : function () {
            var options = {
                params: this.search
            };

            this.$http.get(routePrefix + '/find-users', options).then(function (response) {
                this.tableData = response.body;
            });
        }
    },
    watch : {
        search : {
            handler : function (value, oldValue) {
                this.updateTableView();
            },
            deep : true
        }
    }
});
