<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Should be switched on json_array in future
     *
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User")
     */
    protected $communication;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->communication = new ArrayCollection();
        $this->createdAt     = new \DateTime();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add communication
     *
     * @param \UserBundle\Entity\User $communication
     *
     * @return User
     */
    public function addCommunication(\UserBundle\Entity\User $communication)
    {
        $this->communication[] = $communication;

        return $this;
    }

    /**
     * Remove communication
     *
     * @param \UserBundle\Entity\User $communication
     */
    public function removeCommunication(\UserBundle\Entity\User $communication)
    {
        $this->communication->removeElement($communication);
    }

    /**
     * Get communication
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommunication()
    {
        return $this->communication;
    }
}
