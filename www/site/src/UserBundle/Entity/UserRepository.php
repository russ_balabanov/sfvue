<?php

namespace UserBundle\Entity;

use AppBundle\Filter\UserFilter;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package UserBundle\Entity
 */
class UserRepository extends EntityRepository
{
    /**
     * Returns all non ROLE_ADMIN users
     *
     * @param UserFilter $filter
     *
     * @return array
     */
    public function getUsers(UserFilter $filter = null)
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->select('u.id', 'u.roles', 'u.username', 'u.email', 'u.createdAt')
            ->where($qb->expr()->notLike('u.roles', ':roleAdmin'))
            ->setParameter('roleAdmin', '%"ROLE_ADMIN"%') // <-- It's a bit dirty trick in future better to use json::array
            ->orderBy('u.id', 'asc');

        if ($filter) {
            if ($filter->getUser()) {
                $qb
                    ->andWhere($qb->expr()->like('u.username', ':username'))
                    ->setParameter('username', '%'. $filter->getUser() . '%');
            }

            if ($filter->getRole()) {
                $qb
                    ->andWhere($qb->expr()->like('u.roles', ':role'))
                    ->setParameter('role', '%"' . $filter->getRole() . '"%');
            }

            if ($filter->getIgnoredUserIds()) {
                $qb->andWhere($qb->expr()->notIn('u.id', $filter->getIgnoredUserIds()));
            }
        }

        return $qb->getQuery()->getArrayResult();
    }
}
