<?php

namespace AppBundle\Filter;

use Symfony\Component\HttpFoundation\Request;

class UserFilter
{
    private $user;

    /**
     * @var string
     */
    private $role;

    /**
     * @var array
     */
    private $ignoreUserIds;

    /**
     * UserFilter constructor.
     *
     * @param Request $request
     * @param array $options
     */
    public function __construct(Request $request, array $options = [])
    {
        $this->user           = $request->get('user');
        $this->role           = $request->get('role');
        $this->ignoredUserIds = isset($options['ignore']) ? $options['ignore'] : [];
    }

    /**
     * @return array|mixed
     */
    public function getIgnoredUserIds()
    {
        return $this->ignoredUserIds;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getRole()
    {
        return $this->role;
    }
}
