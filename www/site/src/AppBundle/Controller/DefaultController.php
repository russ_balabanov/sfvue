<?php

namespace AppBundle\Controller;

use AppBundle\Filter\UserFilter;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('::index.html.twig');
    }

    /**
     * @Route("/settings", name="settings")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function settingsAction()
    {
        return $this->render('::settings.html.twig');
    }

    /**
     * @Route("/find-users", name="findUsers")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function findUsersAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->getUsers(new UserFilter($request));

        return new JsonResponse($users);
    }

    /**
     * @param Request $request
     * @param User $user
     *
     * @Route("/update-user/{id}", name="updateUser")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return JsonResponse
     */
    public function updateUser(Request $request, User $user)
    {
        // A small fix for vue-resource. When object is empty
        if ($userFormData = $request->request->get('user')) {
            if (empty($userFormData['roles'])) {
                $request->request->set('user', ['roles' => []]);
            }
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            } else {
                return new JsonResponse(['invalid request', Response::HTTP_BAD_REQUEST]);
            }
        }

        return new JsonResponse(['roles' => $user->getRoles()]);
    }
}
