<?php

namespace AppBundle\Controller;

use AppBundle\Filter\UserFilter;
use AppBundle\Form\CommunicationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Class CommunicationController
 * @package AppBundle\Controller
 *
 * @Route("/communication")
 */
class CommunicationController extends Controller
{
    /**
     * @Route("/", name="communication")
     * @Security("has_role('ROLE_READ')")
     */
    public function communicationAction(Request $request)
    {
        /**
         * @var User $user
         */
        $user  = $this->getUser();

        // Do not fetch current user in the Users list
        $filter = new UserFilter($request, ['ignore' => [$user->getId()]]);
        $users  = $this->getDoctrine()->getRepository(User::class)->getUsers($filter);

        $selectedUsers = [];

        /**
         * @var User $cUser
         */
        foreach ($user->getCommunication() as $cUser) {
            $selectedUsers[$cUser->getId()] = [
                'id'        => $cUser->getId(),
                'username'  => $cUser->getUsername(),
                'email'     => $cUser->getEmail(),
                'createdAt' => $cUser->getCreatedAt()
            ];
        }

        $form = $this->createForm(CommunicationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // Allow form submit only form certain roles
            if (!$this->isGranted('ROLE_EDIT')) {
                throw $this->createAccessDeniedException('Not allowed to edit this form');
            }

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $communicationOrdering = $request->request->get($form->getName())['communication'];

                $userPool = [];
                foreach ($user->getCommunication() as $cUser) {
                    $userPool[$cUser->getId()] = $cUser;
                }

                $user->getCommunication()->clear();
                foreach ($communicationOrdering as $orderId) {
                    $user->getCommunication()->add($userPool[$orderId]);
                }

                $em->flush();

                $this->addFlash('success', 'Communication data has been updated for user ' . $user->getUsername());

                return $this->redirectToRoute('communication');
            }
    }

        $usersData = [];
        foreach ($users as $user) {
            $usersData[$user['id']] = $user;
        }

        return $this->render(':communication:communication.html.twig', [
            'form'          => $form->createView(),
            'users'         => json_encode($usersData),
            'selectedUsers' => json_encode($selectedUsers),
            'isEditAllowed' => (int) $this->isGranted('ROLE_EDIT')
        ]);
    }

    /**
     * @Route("/delete", name="deleteCommunication")
     * @Security("has_role('ROLE_DELETE')")
     */
    public function deleteAction()
    {
        /**
         * @var User $user
         */
        $user  = $this->getUser();

        $user->getCommunication()->clear();
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Communication data has been deleted');

        return $this->redirectToRoute('communication');
    }
}
