<?php

namespace AppBundle\Traits;

trait UserTrait
{
    /**
     * @param $userId
     * @param array $roles
     *
     * @return string
     */
    public function getUpdateRoleSQL($userId, array $roles)
    {
        $roles = serialize($roles);

        $sql = <<<SQL
UPDATE "users" SET roles = '{$roles}' WHERE id = {$userId}
SQL;

        return $sql;
    }
}
