<?php

namespace Application\Migrations;

use AppBundle\Traits\UserTrait;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\User;

class Version20170139133434 extends AbstractMigration implements ContainerAwareInterface
{
    use UserTrait;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $userManipulator = $this->container->get('fos_user.util.user_manipulator');

        for ($i = 1; $i <= 10; $i++) {
            $password = "someUserPass";
            $username = "User_" . $i;
            $email    = sprintf("user%d@gmail.com", $i);
            $isActive = true;

            /**
             * @var User $user
             */
            $user = $userManipulator->create($username, $password, $email, $isActive, false);

            $sql = $this->getUpdateRoleSQL($user->getId(), ['ROLE_READ']);
            $this->addSql($sql);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        for ($i = 1; $i <= 10; $i++) {
            $sql = <<<SQL
DELETE FROM "users" WHERE username = 'User_{$i}' AND email = 'user{$i}@gmail.com';
SQL;
            $this->addSql($sql);
        }
    }
}
