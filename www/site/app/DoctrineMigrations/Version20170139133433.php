<?php

namespace Application\Migrations;

use AppBundle\Traits\UserTrait;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\User;

class Version20170139133433 extends AbstractMigration implements ContainerAwareInterface
{
    use UserTrait;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $userManipulator = $this->container->get('fos_user.util.user_manipulator');
        $adminUsername   = "admin";
        $adminPassword   = "qwerty123";
        $adminEmail      = "russ.developer@gmail.com";
        $isActive        = true;

        /**
         * @var User $user
         */
        $user = $userManipulator->create($adminUsername, $adminPassword, $adminEmail, $isActive, false);

        $sql = $this->getUpdateRoleSQL($user->getId(), ['ROLE_ADMIN']);
        $this->addSql($sql);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $sql = <<<SQL
DELETE FROM "users" WHERE username = 'admin' AND email = 'russ.developer@gmail.com';
SQL;
        $this->addSql($sql);
    }
}
